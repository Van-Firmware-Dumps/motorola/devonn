#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from devonn device
$(call inherit-product, device/motorola/devonn/device.mk)

PRODUCT_DEVICE := devonn
PRODUCT_NAME := lineage_devonn
PRODUCT_BRAND := motorola
PRODUCT_MODEL := moto g power 5G - 2023
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="devonn_g_sys-user 14 U1TOS34M.1-157-5-2 29036 release-keys"

BUILD_FINGERPRINT := motorola/devonn_g_sys/devonn:14/U1TOS34M.1-157-5-2/29036:user/release-keys
