on early-init
    insmod /vendor/lib/modules/sensors_class.ko
    insmod /vendor/lib/modules/sx937x_sar.ko
    exec_background u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules qpnp_adaptive_charge aw9610x leds-gpio moto_mmap_fault
    exec u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -r -d /vendor/lib/modules zram
on post-fs-data
    # calibration
    mkdir /data/vendor/sensor 0774 system system
    # Sensor
    chmod 0660 /dev/hf_manager
    chown system system /dev/hf_manager

    # config fingerprint
    mkdir /data/vendor/.fps 0770 system vendor_fingerp
    mkdir /mnt/vendor/persist/fps 0770 system system
    mkdir /data/vendor/egis 0770 system vendor_fingerp
    mkdir /data/vendor/egis/cqa 0770 system vendor_fingerp
    mkdir /mnt/vendor/persist/egis 0770 system vendor_fingerp

    # Required by touchRec for write the touch data
    mkdir /data/vendor/touchrec 0770 input input
    chown input input /data/vendor/touchrec/bootindex
    chown input input /data/vendor/touchrec/lastbootuuid
    chown input input /data/vendor/touchrec/touch_data.txt
    chmod 0664 /data/vendor/touchrec/touch_data.txt

# Use dirty bytes
    write /proc/sys/vm/dirty_background_bytes 26214400
    write /proc/sys/vm/dirty_bytes 104857600

on late-init
    #moto algo params
    chmod 0660 /sys/bus/platform/drivers/mtk_nanohub/algo_params
    chown system system /sys/bus/platform/drivers/mtk_nanohub/algo_params

    chown system system /sys/class/sensors/capsense_bottom_left/enable
    chown system system /sys/class/sensors/capsense_bottom_left/poll_delay
    chown system system /sys/class/sensors/capsense_bottom_right/enable
    chown system system /sys/class/sensors/capsense_bottom_right/poll_delay
    chown system system /sys/class/sensors/capsense_top_left/enable
    chown system system /sys/class/sensors/capsense_top_left/poll_delay
    chown system system /sys/class/sensors/capsense_top_mid/enable
    chown system system /sys/class/sensors/capsense_top_mid/poll_delay
    chown system system /sys/class/sensors/capsense_top_right/enable
    chown system system /sys/class/sensors/capsense_top_right/poll_delay

    exec_background u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules aw862x.ko
    exec u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules moto_mm moto_swap

service capsense_reset /vendor/bin/capsense_reset
    class core
    user system
    group system input
    disabled

on property:sys.boot_completed=1
    start capsense_reset
    chown system system /sys/class/touchscreen/primary/stylus_mode
    chmod 0660 /sys/class/touchscreen/primary/stylus_mode

# Turn on led to indicate device on in factory mode
on property:ro.bootmode=mot-factory
    write /sys/class/leds/charging/brightness 255

on early-boot
    # Permission for Health Storage HAL
    chown system system /sys/devices/platform/soc/112b0000.ufshci/manual_gc
    chown system system /sys/devices/platform/soc/112b0000.ufshci/ufshid/trigger

on boot
    write /sys/class/i2c-dev/i2c-9/device/9-0030/wireless_fw_update 1
    # Set wls perms for HAL
    chown system system /sys/class/power_supply/wireless/device/pen_control
    chown system system /sys/class/power_supply/wireless/device/tx_mode
    chown system system /sys/class/power_supply/wireless/device/wls_input_current_limit
    chown system system /sys/class/power_supply/wireless/device/folio_mode
    chmod 0660 /sys/class/power_supply/wireless/device/pen_control
    chmod 0660 /sys/class/power_supply/wireless/device/tx_mode
    chmod 0660 /sys/class/power_supply/wireless/device/wls_input_current_limit
    chmod 0660 /sys/class/power_supply/wireless/device/folio_mode
    # change permission for capsensor
    chown system system /sys/class/capsense/reset
    chown system system /sys/class/capsense/int_state
    chown radio system /sys/class/capsense/reg
    chown radio system /sys/class/capsense/fw_download_status
    chmod 0660 /sys/class/capsense/reset
    chmod 0660 /sys/class/capsense/int_state
    chmod 0660 /sys/class/capsense/reg
    chmod 0660 /sys/class/capsense/fw_download_status

    # Change ownership and permission for cp-standalone factory testing
    chown system system /sys/class/power_supply/cp-standalone/voltage_now
    chown system system  /sys/class/power_supply/cp-standalone/device/force_chg_auto_enable
    chmod 0644 /sys/class/power_supply/cp-standalone/voltage_now
    # Set adaptive charging perms for HAL
    chown system system /sys/module/qpnp_adaptive_charge/parameters/upper_limit
    chown system system /sys/module/qpnp_adaptive_charge/parameters/lower_limit
    # touch api
    chown system system /sys/class/touchscreen/primary/interpolation
    chmod 0660 /sys/class/touchscreen/primary/interpolation
    chown system system /sys/class/touchscreen/primary/first_filter
    chmod 0660 /sys/class/touchscreen/primary/first_filter
    chown system system /sys/class/touchscreen/primary/edge
    chmod 0660 /sys/class/touchscreen/primary/edge
    chown system system /sys/class/touchscreen/primary/gesture
    chmod 0660 /sys/class/touchscreen/primary/gesture
    chmod 0664 /proc/cts_tool

    # set aw smart pa node can be accessed by audio group
    chown root audio /sys/devices/platform/11ed4000.i2c/i2c-9/9-0034/cali_re
    chmod 0664 /sys/devices/platform/11ed4000.i2c/i2c-9/9-0034/cali_re

    write /sys/block/sdc/queue/scheduler bfq
    write /sys/block/sdc/queue/iosched/slice_idle 0

on fs
    exec_background u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules mmi_relay chipone_tddi_fhd_mmi focaltech_ft8726_mmi

on property:ro.bootmode=mot-factory
    write /sys/class/leds/charging/brightness 255

on property:sys.boot_completed=1
    chmod 0644 /data/vendor/camera_dump/mot_gt24p128e_s5kjn1_eeprom.bin
    chmod 0644 /data/vendor/camera_dump/mot_gt24p64ba2_hi1634q_eeprom.bin
    chmod 0644 /data/vendor/camera_dump/mot_devonn_gc02m1_otp.bin
    chmod 0644 /data/vendor/camera_dump/mot_devonn_ov02b10_otp.bin

on moto-post-fs-data-fs-tune
    write /sys/block/${dev.mnt.dev.system_ext}/queue/read_ahead_kb 2048
    write /sys/block/${dev.mnt.dev.vendor}/queue/read_ahead_kb 2048
    write /sys/block/${dev.mnt.dev.product}/queue/read_ahead_kb 2048
    write /sys/block/${dev.mnt.dev.data}/queue/read_ahead_kb 2048
    write /sys/block/${dev.mnt.dev.root}/queue/read_ahead_kb 2048
    write /sys/block/${dev.mnt.dev.vendor_dlkm}/queue/read_ahead_kb 2048

on moto-boot-completed-fs-tune
    write /sys/block/dm-0/queue/read_ahead_kb 512
    write /sys/block/dm-1/queue/read_ahead_kb 512
    write /sys/block/dm-2/queue/read_ahead_kb 512
    write /sys/block/dm-3/queue/read_ahead_kb 512
    write /sys/block/dm-4/queue/read_ahead_kb 512
    write /sys/block/dm-5/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.system_ext}/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.vendor}/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.product}/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.data}/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.root}/queue/read_ahead_kb 512
    write /sys/block/${dev.mnt.dev.vendor_dlkm}/queue/read_ahead_kb 512
